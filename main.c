#include <stdio.h>
#include <stdlib.h>

void swap(int *x, int *y) {
    int z = *x;
    *x = *y;
    *y = z;
}

void sort(int *a, int left, int right) {
    int l = left;
    int r = right;
    int pilot = a[(left + right) / 2];
    do {
        while (a[l] < pilot)
            l++;
        while (a[r] > pilot)
            r--;
        if (l > r) {
            break;
        }
        if (a[l] > a[r]) {
            swap(&a[l], &a[r]);
        }
        l++;
        r--;
    } while (l <= r);
    if (l < right)
        sort(a, l, right);
    if (left < r)
        sort(a, left, r);
}

int main() {

    FILE *in = fopen("in.txt", "r");
    FILE *out =fopen("out.txt", "w");

    int n;
    if(fscanf(in,"%d\n", &n) == EOF){
        fprintf(out,"bad input");
        return 0;
    }
    
    int *a = calloc((size_t)n, sizeof(int));
    if(a == NULL){
        perror("Memory Err");
        return 0;
    }
    
    for (int i = 0; i < n; i++) {
        if(fscanf(in,"%d", &a[i]) == EOF){
            fprintf(out,"bad input");
            return 0;
        }
    }
    sort(a, 0, n - 1);
    for (int j = 0; j < n; ++j) {
        fprintf(out,"%d ", a[j]);
    }
    free(a);

    fclose(in);
    fclose(out);

    return 0;
}